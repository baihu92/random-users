import 'package:flutter/cupertino.dart';

class Picture {
  final String large;
  final String medium;
  final String thumbnail;

  Picture({
    @required this.large,
    @required this.medium,
    @required this.thumbnail,
  });

  factory Picture.fromMap(dynamic map) {
    return Picture(
      large: map['large'],
      medium: map['medium'],
      thumbnail: map['thumbnail'],
    );
  }
}
