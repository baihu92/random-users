import 'package:random_users/utils/string_utils.dart';

class Location {
  String street;
  String city;
  String state;
  String postcode;

  String get fullAddress => '$postcode, $state, $city, $street';

  Location({this.street, this.city, this.state, this.postcode});

  @override
  String toString() {
    return 'Location{street: $street, city: $city, state: $state, '
        'postcode: $postcode}';
  }

  factory Location.fromMap(dynamic map) {
    return Location(
      street: capitalize(map['street']),
      city: capitalize(map['city']),
      state: capitalize(map['state']),
      postcode: map['postcode'].toString(),
    );
  }
}
