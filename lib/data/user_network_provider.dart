import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:random_users/data/user.dart';

class UserNetworkProvider {
  Dio dio;

  UserNetworkProvider({@required this.dio}) : assert(dio != null);

  /// Возвращает список пользователей.
  /// [limit] максимальное количество пользователей в возвращаемом списке.
  Future<List<User>> getUsers([int limit = 10]) async {
    var response = await dio.get('/api', queryParameters: {'results': limit});
    List<dynamic> results = response.data['results'];
    return results.map((item) => User.fromMap(item)).toList();
  }
}
