import 'package:flutter/cupertino.dart';
import 'package:random_users/data/location.dart';
import 'package:random_users/data/picture.dart';
import 'package:random_users/utils/string_utils.dart';

class User {
  final String firstName;
  final String lastName;
  final Location location;
  final String email;
  final String phone;
  final Picture picture;

  String get fullName => '$firstName $lastName';

  User({
    @required this.firstName,
    @required this.lastName,
    @required this.location,
    @required this.email,
    @required this.phone,
    @required this.picture,
  });

  factory User.fromMap(dynamic map) {
    var name = map['name'];
    return User(
      firstName: capitalize(name['first']),
      lastName: capitalize(name['last']),
      location: Location.fromMap(map['location']),
      email: map['email'],
      phone: map['phone'],
      picture: Picture.fromMap(map['picture']),
    );
  }

  @override
  String toString() {
    return 'User{firstName: $firstName, lastName: $lastName, '
        'location: $location, email: $email, phone: $phone}';
  }
}
