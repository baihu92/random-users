import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:random_users/presentation/users_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Dio _dio;

  @override
  void initState() {
    _dio = Dio(BaseOptions(baseUrl: 'https://randomuser.me'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Random Users',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: UsersPage(dio: _dio),
    );
  }

  @override
  void dispose() {
    _dio.clear();
    super.dispose();
  }
}
