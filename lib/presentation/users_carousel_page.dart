import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:random_users/data/user.dart';
import 'package:random_users/data/user_network_provider.dart';

class UsersCarouselPage extends StatefulWidget {
  final List<User> users;
  final UserNetworkProvider networkProvider;
  final int index;

  UsersCarouselPage(
      {@required this.networkProvider,
      @required this.users,
      @required this.index});

  @override
  _UsersCarouselPageState createState() => _UsersCarouselPageState();
}

class _UsersCarouselPageState extends State<UsersCarouselPage> {
  List<User> get _users => widget.users;

  UserNetworkProvider get _provider => widget.networkProvider;
  PageController _controller;
  int index = 0;
  dynamic error;

  @override
  initState() {
    this.index = widget.index;
    _controller = PageController(
      initialPage: index,
      viewportFraction: 0.8,
    );
    _controller.addListener(() {
      final position = _controller.position;
      if (error == null && position.pixels == position.maxScrollExtent) {
        _fetchUsers();
      }
    });
    super.initState();
  }

  void _fetchUsers() {
    _provider.getUsers().then((list) {
      setState(() {
        _users.addAll(list);
      });
    }).catchError((error) {
      setState(() {
        this.error = error;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(0, 0, 0, .6),
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: PageView.builder(
                  itemBuilder: _buildItem,
                  controller: _controller,
                  itemCount: _users.length + 1,
                  onPageChanged: (value) {
                    setState(() {
                      this.index = value;
                    });
                  },
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.0),
                  child: Text(
                    '${index + 1} из ${_users.length}',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onTapFailure() {
    setState(() {
      error = null;
    });
    _fetchUsers();
  }

  Widget _buildItem(BuildContext context, int index) {
    if (index < _users.length) {
      return _UserItem(user: _users[index]);
    }

    if (error != null) {
      return _FailureItem(onTap: _onTapFailure);
    }

    return _LoadingItem();
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }
}

/// Ячейка с ошибкой.
class _FailureItem extends StatelessWidget {
  final VoidCallback onTap;

  _FailureItem({@required this.onTap});

  @override
  Widget build(BuildContext context) {
    const radius = Radius.circular(8);
    return Padding(
      padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 60.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: radius, topRight: radius),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Произошла ошибка',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
            ),
            MaterialButton(
              child: Text(
                'Повторить',
                style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
              onPressed: onTap,
            ),
          ],
        ),
      ),
    );
  }
}

/// Ячейка загрузки.
class _LoadingItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const radius = Radius.circular(8);
    return Padding(
      padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 60.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: radius, topRight: radius),
        ),
        child: Center(child: CircularProgressIndicator()),
      ),
    );
  }
}

/// Ячейка пользователя.
class _UserItem extends StatelessWidget {
  static const _radius = Radius.circular(4.0);
  static const _padding = 18.0;

  final User user;

  _UserItem({
    @required this.user,
  });

  Widget _createTitle(String text) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 18.0),
      child: Text(
        text,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  Widget _createValue(String text) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 18.0),
      child: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 60.0),
      child: Card(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: _radius,
                  topRight: _radius,
                ),
                child: Image.network(
                  user.picture.large,
                  fit: BoxFit.fill,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(_padding),
                child: Text(
                  user.fullName,
                  style: TextStyle(fontSize: 24),
                ),
              ),
              Container(
                color: Colors.grey.shade200,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: _padding),
                    _createTitle('Address:'),
                    _createValue(user.location.fullAddress),
                    SizedBox(height: _padding),
                    _createTitle('Email:'),
                    _createValue(user.email),
                    SizedBox(height: _padding),
                    _createTitle('Phone:'),
                    _createValue(user.phone),
                    SizedBox(height: _padding),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
