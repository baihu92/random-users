import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:random_users/data/user.dart';
import 'package:random_users/data/user_network_provider.dart';
import 'package:random_users/presentation/users_carousel_page.dart';

/// Страница со списком пользователей.
class UsersPage extends StatefulWidget {
  final Dio dio;

  UsersPage({@required this.dio});

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  Dio get _dio => widget.dio;
  UserNetworkProvider _provider;
  ScrollController _scrollController = ScrollController();
  final users = List<User>();
  dynamic error;

  void _fetchUsers() {
    _provider.getUsers().then((list) {
      setState(() {
        users.addAll(list);
      });
    }).catchError((error) {
      setState(() {
        this.error = error;
      });
    });
  }

  @override
  void initState() {
    _provider = UserNetworkProvider(dio: _dio);
    _fetchUsers();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scrollListener() {
    final position = _scrollController.position;
    if (position.pixels == position.maxScrollExtent && error == null) {
      _fetchUsers();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Users Page'),
      ),
      body: _createBody(),
    );
  }

  Widget _createBody() {
    if (users.isNotEmpty) {
      return ListView.builder(
        controller: _scrollController,
        itemCount: users.length + 1,
        itemBuilder: _itemBuilder,
      );
    }

    if (error == null) {
      return _LoadingItem();
    }

    return _FailureItem(
      onPressed: _onTapFailure,
    );
  }

  void _onSelectedUserHandler(User user, int index) {
    Navigator.of(context).push(PageRouteBuilder(
      opaque: false,
      pageBuilder: (context, anim1, anim2) => UsersCarouselPage(
        networkProvider: _provider,
        users: users,
        index: index,
      ),
    ));
  }

  void _onTapFailure() {
    setState(() {
      error = null;
    });
    _fetchUsers();
  }

  Widget _itemBuilder(BuildContext context, int index) {
    if (error != null && index == users.length) {
      return _FailureItem(onPressed: _onTapFailure);
    }

    if (users.isEmpty) {
      return _LoadingItem();
    }

    if (index == users.length) {
      return _LoadingItem();
    }

    return _UserItem(users[index], index, _onSelectedUserHandler);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}

/// Ячейка загрузки.
class _LoadingItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(18.0),
        child: CircularProgressIndicator(),
      ),
    );
  }
}

/// Сигнатура для [_UserItem.onSelectedUserCallback].
typedef _OnSelectedUserCallback = void Function(User user, int index);

/// Ячейка пользователя.
class _UserItem extends StatelessWidget {
  final User user;
  final int index;
  final _OnSelectedUserCallback onSelectedUserCallback;

  _UserItem(this.user, this.index, this.onSelectedUserCallback);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: () => onSelectedUserCallback(user, index),
        contentPadding: EdgeInsets.all(8.0),
        leading: Image.network(
          user.picture.medium,
          width: 56,
          height: 56,
        ),
        title: Text(user.fullName),
        subtitle: Text(user.location.fullAddress),
      ),
    );
  }
}

/// Ячейка ошибки.
class _FailureItem extends StatelessWidget {
  final VoidCallback onPressed;

  _FailureItem({@required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Произошла ошибка',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
            ),
            MaterialButton(
              child: Text(
                'Повторить',
                style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
              onPressed: onPressed,
            ),
          ],
        ),
      ),
    );
  }
}
