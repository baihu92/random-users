import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:random_users/data/user.dart';

const String sampleSource = '''    {
      "gender": "female",
      "name": {
        "title": "miss",
        "first": "christina",
        "last": "lawson"
      },
      "location": {
        "street": "7427 bridge road",
        "city": "westminster",
        "state": "herefordshire",
        "postcode": "J12 9QD",
        "coordinates": {
          "latitude": "6.9499",
          "longitude": "59.4324"
        },
        "timezone": {
          "offset": "+2:00",
          "description": "Kaliningrad, South Africa"
        }
      },
      "email": "christina.lawson@example.com",
      "login": {
        "uuid": "3808b1e7-c81b-48af-b5d2-b6ca53422dd8",
        "username": "bluepanda459",
        "password": "spiderma",
        "salt": "GeRIN0Tv",
        "md5": "5cfd33cd79fd1bc3ccbfa2db33b44663",
        "sha1": "47c58e45c09e8b56a091cc029b81927897af57bd",
        "sha256": "9ce9cac614e2f9aea6870cf7fc91742e64fb4e5925a4d0a8ffa746180e0642ee"
      },
      "dob": {
        "date": "1986-12-07T06:27:49Z",
        "age": 32
      },
      "registered": {
        "date": "2012-06-07T10:02:11Z",
        "age": 7
      },
      "phone": "015242 08136",
      "cell": "0767-708-477",
      "id": {
        "name": "NINO",
        "value": "LR 82 40 99 C"
      },
      "picture": {
        "large": "https://randomuser.me/api/portraits/women/30.jpg",
        "medium": "https://randomuser.me/api/portraits/med/women/30.jpg",
        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/30.jpg"
      },
      "nat": "GB"
    }''';

void main() {
  group('User', () {
    User user;

    setUp(() {
      var source = jsonDecode(sampleSource);
      user = User.fromMap(source);
    });

    test('fromMap should initilize firstName', () {
      expect(user.firstName, 'christina');
    });

    test('fromMap should initilize lastName', () {
      expect(user.lastName, 'lawson');
    });

    test('fromMap should initilize email', () {
      expect(user.email, 'christina.lawson@example.com');
    });

    test('fromMap should initilize email', () {
      expect(user.phone, 'christina.lawson@example.com');
    });
  });
}
