import 'package:flutter_test/flutter_test.dart';
import 'package:random_users/data/location.dart';
import 'dart:convert';

const String testSource = '''{
        "street": "7427 bridge road",
        "city": "westminster",
        "state": "herefordshire",
        "postcode": "J12 9QD",
        "coordinates": {
          "latitude": "6.9499",
          "longitude": "59.4324"
        }
        }''';

void main() {
  group('Location', () {
    test('fromMap should initialize street', () {
      var map = jsonDecode(testSource);
      var location = Location.fromMap(map);

      expect(location.street, equals('7427 bridge road'));
    });

    test('fromMap should initialize city', () {
      var map = jsonDecode(testSource);
      var location = Location.fromMap(map);

      expect(location.city, equals('westminster'));
    });

    test('fromMap should initialize state', () {
      var map = jsonDecode(testSource);
      var location = Location.fromMap(map);

      expect(location.state, equals('herefordshire'));
    });

    test('fromMap should initialize postcode', () {
      var map = jsonDecode(testSource);
      var location = Location.fromMap(map);

      expect(location.postcode, equals('J12 9QD'));
    });

    test('fromMap should initialize postcode with number', () {
      var map = {'postcode': 101};
      var location = Location.fromMap(map);
      expect(location.postcode, '101');
    });
  });
}
